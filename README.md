# README #

### What is this repository for? ###

* This is a RSS News Reader tool.
* For this project I used Visual Studio 2017, .NET Framework 4.5.2
* This project is made using ASP.Net MVC 5, ASP.Net Web Api, Typescript, jQuery, Knockout JS, Lodash, Toastr, Bootstrap.

### How do I get the set up? ###

* Just download or clone this repository.
* Open the project with Visual Studio 2017 and run it.

# EXERCISE #

## IMPACT ##

### ASP.NET kodeopgaver ###

Formål

Formålet med opgaverne er at give et fælles udgangspunkt for diskussion af, hvordan du opbygger din

kode, din tilgang til løsning af opgaver og din forståelse af .NET generelt. Til den efterfølgende samtale vil vi

tale om:

* Hvordan har du grebet opgaven an?
* Hvor lang tid har du brugt på de enkelte delopgaver og hvordan har du prioriteret tiden i mellem de forskellige delopgaver?
* Hvorfor har du bygget koden op, som du har?
* Hvad har overrasket dig mest under løsning af opgaven?

Det forventes ikke at du kan løse alle opgaver indenfor den angivne tidsramme.

Når tidsrammen er udløbet skal du zippe din projektmappe, uploade den til [File Dropper](http://www.filedropper.com/)

og sende os linket, så vi kan downloade den. På dette tidspunkt skal projektet kunne compiles uden fejl.

Opgaver

### A: RSS Viser ###

Denne opgave går ud på at hente nyheder i RSS format fra en remote server og vise dem til brugeren. Den

skal løses i ASP.NET ved brug af C# og Javascript. Valg af ASP.NET Webforms eller ASP.NET MVC er op til dig.

1. Lav en visning af et RSS nyhedsitem, hvor man mindst kan læse:
    * Title
    * Publication date
2. Link til hver nyhedsitem så slutbrugeren kan navigere til nyhedsitem’et.
3. Gør RSS URL konfigurerbar, således at man som slutbruger selv kan ændre denne.
4. Giv brugeren mulighed for at angive hvordan listen skal sorteres.

### B: Egen collection ###

Implementér din egen collection (uden brug af collection klasser fra .NET framework)

Info

* Du kan benytte Visual Studio 2010/2012/2013/2015/2017 til opgaven.