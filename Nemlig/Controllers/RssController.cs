﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Nemlig.Helpers;
using Nemlig.Models;

namespace Nemlig.Controllers
{
    public class RssController : ApiController
    {
        // GET api/<controller>
        public async Task<List<RssFeedElement>> Get(string url)
        {
            if (string.IsNullOrEmpty(url))
                return null;
            
            var rssReader = new RssDownloaderHelper(url);

            var elements = await rssReader.GetElements();

            return elements;
        }
    }
}