﻿module Global.Models {
    export class RssFeed {

        url: KnockoutObservable<string>;
        news: KnockoutObservableArray<RssFeedElement>;
        isLoading: KnockoutObservable<boolean>;

        constructor() {
            this.url = ko.observable("http://feeds.bbci.co.uk/news/rss.xml?edition=int");
            this.news = ko.observableArray([]);
            this.isLoading = ko.observable(false);
        }

        readFeed() {
            // Set loading icon.
            this.isLoading(true);

            // Call the API.
            $.ajax({
                url: "/api/Rss?url=" + this.url(),
                method: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done((result: RssFeedElementNet[]) => {
                // Put the elements in the array.
                this.news(_.map(result, rfe => new RssFeedElement(rfe)));
                // Hide loading icon.
                this.isLoading(false);
            });
        }

        sortByTitle() {
            this.news(_.sortBy(this.news(), (n) => { return n.title() }));
        }

        sortByDate() {
            this.news(_.sortBy(this.news(), (n) => { return n.publicationDate() }));
        }
    }
}