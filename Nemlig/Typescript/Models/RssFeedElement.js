var Global;
(function (Global) {
    var Models;
    (function (Models) {
        var RssFeedElement = (function () {
            function RssFeedElement(m) {
                this.title = ko.observable(m.Title);
                this.publicationDate = ko.observable(m.PublicationDate);
                this.description = ko.observable(m.Description);
                this.link = ko.observable(m.Link);
            }
            RssFeedElement.prototype.toNet = function () {
                var model = new RssFeedElementNet();
                model.Title = this.title();
                model.PublicationDate = this.publicationDate();
                model.Description = this.description();
                return model;
            };
            return RssFeedElement;
        }());
        Models.RssFeedElement = RssFeedElement;
        var RssFeedElementNet = (function () {
            function RssFeedElementNet() {
            }
            return RssFeedElementNet;
        }());
        Models.RssFeedElementNet = RssFeedElementNet;
    })(Models = Global.Models || (Global.Models = {}));
})(Global || (Global = {}));
//# sourceMappingURL=RssFeedElement.js.map