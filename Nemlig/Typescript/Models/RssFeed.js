var Global;
(function (Global) {
    var Models;
    (function (Models) {
        var RssFeed = (function () {
            function RssFeed() {
                this.url = ko.observable("http://feeds.bbci.co.uk/news/rss.xml?edition=int");
                this.news = ko.observableArray([]);
                this.isLoading = ko.observable(false);
            }
            RssFeed.prototype.readFeed = function () {
                var _this = this;
                // Set loading icon.
                this.isLoading(true);
                // Call the API.
                $.ajax({
                    url: "/api/Rss?url=" + this.url(),
                    method: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                }).done(function (result) {
                    // Put the elements in the array.
                    _this.news(_.map(result, function (rfe) { return new Models.RssFeedElement(rfe); }));
                    // Hide loading icon.
                    _this.isLoading(false);
                });
            };
            RssFeed.prototype.sortByTitle = function () {
                this.news(_.sortBy(this.news(), function (n) { return n.title(); }));
            };
            RssFeed.prototype.sortByDate = function () {
                this.news(_.sortBy(this.news(), function (n) { return n.publicationDate(); }));
            };
            return RssFeed;
        }());
        Models.RssFeed = RssFeed;
    })(Models = Global.Models || (Global.Models = {}));
})(Global || (Global = {}));
//# sourceMappingURL=RssFeed.js.map