﻿module Global.Models {
    export class RssFeedElement {
        title: KnockoutObservable<string>;
        publicationDate: KnockoutObservable<Date>;
        description: KnockoutObservable<string>;
        link: KnockoutObservable<string>;

        constructor(m: RssFeedElementNet) {
            this.title = ko.observable(m.Title);
            this.publicationDate = ko.observable(m.PublicationDate);
            this.description = ko.observable(m.Description);
            this.link = ko.observable(m.Link);
        }
        
        toNet(): RssFeedElementNet {
            var model = new RssFeedElementNet();
            model.Title = this.title();
            model.PublicationDate = this.publicationDate();
            model.Description = this.description();
            return model;
        }
    }

    export class RssFeedElementNet {
        Title: string;
        PublicationDate: Date;
        Description: string;
        Link: string;
    }
}