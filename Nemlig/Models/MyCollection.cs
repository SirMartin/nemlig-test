﻿using System;

namespace Nemlig.Models
{
    public class MyCollection<T>
    {
        public MyCollection()
        {
            items = new T[0];
        }

        private T[] items;

        public bool AddItem(T item)
        {
            try
            {
                Array.Resize(ref items, items.Length + 1);
                items[items.Length - 1] = item;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool RemoveItem(T item)
        {
            try
            {
                int indexToRemove = Array.IndexOf(items, item);
                RemoveItemByIndex(indexToRemove);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool RemoveItemByIndex(int index)
        {
            try
            {
                bool isRemoved = false;
                var newArray = new T[items.Length - 1];
                for (var i = 0; i < newArray.Length; i++)
                {
                    if (i == index)
                    {
                        isRemoved = true;
                        continue;
                    }
                    newArray[i] = items[isRemoved ? i : (i - 1)];
                }
                items = newArray;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public T GetElementByIndex(int index)
        {
            return items[index];
        }
    }
}