﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;
using Nemlig.Models;

namespace Nemlig.Helpers
{
    public class RssDownloaderHelper : RssReader
    {
        public RssDownloaderHelper(string url):base(url)
        {
        }

        public async Task<List<RssFeedElement>> GetElements()
        {
            var webClient = new WebClient();

            string result = await webClient.DownloadStringTaskAsync(Uri);

            var document = XDocument.Parse(result);

            return (from descendant in document.Descendants("item")
                select new RssFeedElement()
                {
                    Description = descendant.Element("description")?.Value,
                    Title = descendant.Element("title")?.Value,
                    PublicationDate = DateTime.Parse(descendant.Element("pubDate")?.Value),
                    Link = descendant.Element("link")?.Value
                }).ToList();
        }
    }
}