﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;
using Nemlig.Models;

namespace Nemlig.Helpers
{
    public class RssMyCollectionReader : RssReader
    {
        public RssMyCollectionReader(string url):base(url)
        {
        }

        public async Task<MyCollection<RssFeedElement>> GetElements()
        {
            var webClient = new WebClient();

            string result = await webClient.DownloadStringTaskAsync(Uri);

            var document = XDocument.Parse(result);

            var myCollection = new MyCollection<RssFeedElement>();
            foreach (var descendant in document.Descendants("item"))
            {
                var item = new RssFeedElement()
                {
                    Description = descendant.Element("description")?.Value,
                    Title = descendant.Element("title")?.Value,
                    PublicationDate = DateTime.Parse(descendant.Element("pubDate")?.Value),
                    Link = descendant.Element("link")?.Value
                };
                myCollection.AddItem(item);
            }

            return myCollection;
        }
    }
}