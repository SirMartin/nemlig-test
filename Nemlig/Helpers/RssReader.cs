﻿using System;

namespace Nemlig.Helpers
{
    public class RssReader
    {
        public string Url { get; set; }
        public Uri Uri => new Uri(Url);

        public RssReader(string url)
        {
            Url = url;
        }
    }
}